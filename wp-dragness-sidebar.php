<?php
/*
Plugin Name: Dragness Sidebar
Description: Add sidebar support to the dragness theme
Author: Dave Mainville
Author URI: http://superdave2u.com
*/
class WMDragnessSidebar {
	function __construct() {
		$this->init();
	}
	function init() {
		// add support to the theme for widgets :
		add_action('widgets_init',array($this,'register_sidebars'));
		if ( is_active_sidebar( 'sidebar-1' ) ) {
			// hook -- CSS -- before content
			add_action('spyropress_before_post_content',array($this,'inject_style_before_content'));
			
			// hook -- HTML -- before content
			add_action('spyropress_before_post_content',array($this,'inject_before_content'));
			// hook into spyropress_after_main_container
			add_action('spyropress_after_post_content',array($this,'inject_after_content'));
		}
	}
	function register_sidebars() {
		register_sidebar( array(
			'name'          => __( 'Main Sidebar', 'dragness' ),
			'id'            => 'sidebar-1',
			'description'   => __( 'Appears to the right of content on pages.', 'dragness' ),
			'before_widget' => '<aside id="%1$s" class="widget %2$s">',
			'after_widget'  => '</aside>',
			'before_title'  => '<h4 class="widget-title">',
			'after_title'   => '</h4>',
		) );
	}

	function inject_style_before_content() {
		$contentWidthPct  = 70;
		$contentMarginPct = 5;
		
		$footerBucketCheck = array_pop(get_post_custom_values('footer-bucket'));
		if(!is_front_page() && !$footerBucketCheck ) :
		?>
		<!-- before content -->
		<style>
			@media screen and (min-width: 980px) {
				#left-content > .container {
					width: <?php echo $contentWidthPct; ?>%;
					float: left;
				}
				#left-content > .container .container {
					width: 100%;
				}
				#right-sidebar {
					float:left;
					margin-right: <?php echo $contentMarginPct;?>%;
					width: <?php echo 100 - $contentWidthPct - $contentMarginPct;?>%;
				}
			}
		</style>
		<?php
		endif;
	}
	function inject_before_content() {
		?>
		<div id="left-content">
		<?php		
	}
	
	function inject_after_content() {
		?>
		<!-- after content -->
		</div>
		<div id="right-sidebar" class="widget-area">
		<!-- sidebar -->
			<?php dynamic_sidebar( 'sidebar-1' ); ?>
		</div>
		<?php		
	}
}
$WMDragnessSidebar = new WMDragnessSidebar();
?>